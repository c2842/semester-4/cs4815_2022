#include <GL/glut.h>
#include <stdlib.h>
#include <math.h>

#define LINE_CHART 1
#define PIE_CHART 2
#define BAR_CHART 3

class scrPt {
public:
    GLint x, y;
};

const GLdouble twoPi = 6.283185;
GLint offsetX = 0, offsetY = 0;
GLint zoomVal = 1;
int chartType = PIE_CHART;

void circleMidpoint (scrPt, GLint); // fn. defined in circle.cc

GLsizei winWidth = 400, winHeight = 300; // Initial display window size.

GLubyte label[36] = {'J', 'a', 'n', 'F', 'e', 'b', 'M', 'a', 'r',
		     'A', 'p', 'r', 'M', 'a', 'y', 'J', 'u', 'n',
		     'J', 'u', 'l', 'A', 'u', 'g', 'S', 'e', 'p',
		     'O', 'c', 't', 'N', 'o', 'v', 'D', 'e', 'c'};

GLint dataValue[12] = {420, 342, 324, 310, 262, 185,
		       190, 196, 217, 240, 213, 438};

void init (void)
{
    glClearColor(1.0, 1.0, 1.0, 1.0);

    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(0.0, (GLdouble)winWidth, 0.0, (GLdouble)winHeight);
}

void barChart (void)
{
    GLint xRaster = 20, yRaster = 20;
    GLint month, k;
    GLfloat chartWidth = 10 + 12 * 50; // Set chart width ratio to months * bar width + space at the end
    GLfloat chartHeight = 470.0f; // Set chart height ratio a bit higher than the largest value in dataValue[]

    glColor3f(1.0, 0.0, 0.0); //  Set bar color to red.

    for (k = 0; k < 12; k++)
        glRecti(offsetX + ((20 + k * 50) * winWidth / chartWidth), offsetY + (50 / chartHeight * winHeight), offsetX + ((40 + k * 50) * winWidth / chartWidth ), offsetY + (dataValue [k] * winHeight / chartHeight)); // Take window width and height into account so that the chart will resize with the window

    glColor3f(0.0, 0.0, 0.0); //  Set text color to black.

    for (month = 0; month < 12; month++) //  Display chart labels.
    {
        glRasterPos2i(offsetX + (xRaster * winWidth / chartWidth ), offsetY + (yRaster * winHeight / chartHeight)); 
    
        for (k = 3 * month; k < 3 * month + 3; k++)
            glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, label[k]);

        xRaster += 50;
    }
}

void lineChart (void)
{
    GLint x = 30;
    GLint xRaster = 25, yRaster = 20;
    GLfloat chartWidth = 10 + 12 * 50; // Set chart width ratio to months * bar width + space at the end
    GLfloat chartHeight = 470.0f; // Set chart height ratio a bit higher than the largest value in dataValue[]

    glClear(GL_COLOR_BUFFER_BIT);	// clear display window

    glColor3f(0.0, 0.0, 1.0); // set line colour to blue

    glBegin(GL_LINE_STRIP);

    for (int k = 0; k < 12; k++)
        glVertex2i(offsetX + ((x + k * 50) * winWidth / chartWidth ), offsetY + (dataValue[k] * winHeight / chartHeight));

    glEnd();

    glColor3f(1.0, 0.0, 0.0); // Set marker colour to red

    for (int k = 0; k < 12; k++)
    {
        glRasterPos2i(offsetX + ((xRaster + k * 50) * winWidth / chartWidth ), offsetY + ((dataValue[k] - 4) * winHeight / chartHeight ));
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, '*');
    }

    glColor3f(0.0, 0.0, 0.0); // black
    xRaster = 20;

    for (int m = 0; m < 12; m++)
    {
        glRasterPos2i(offsetX + (xRaster * winWidth / chartWidth ), offsetY + (yRaster * winHeight / chartHeight ));

        for (int k = 3 * m; k < 3 * m + 3; k++)
            glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, label[k]);

        xRaster += 50;
    }
}

void pieChart (void)
{
    scrPt circCtr, piePt;
    GLint radius = (winWidth < winHeight ? winWidth : winHeight) / 4; // Circle radius.

    GLdouble sliceAngle, previousSliceAngle = 0.0;

    GLint k, nSlices = 12; // Number of Slices. 
    GLfloat dataValues[12] = {10.0, 7.0, 13.0, 5.0, 13.0, 14.0,
                               3.0, 16, 5.0, 3.0, 17.0, 8.0};
    GLfloat dataSum = 0.0;
 
    circCtr.x = offsetX + (winWidth / 2); // Circle center position.
    circCtr.y = offsetY + (winHeight / 2);
    circleMidpoint (circCtr, radius); // Call midpoint circle-plot routine.

    for (k = 0; k < nSlices; k++)
        dataSum += dataValues[k];

    for (k = 0; k < nSlices; k++) {
        sliceAngle = twoPi * dataValues[k] / dataSum + previousSliceAngle;
        piePt.x = ((GLint)(circCtr.x + radius * cos(sliceAngle)));
        piePt.y = ((GLint)(circCtr.y + radius * sin(sliceAngle)));

        glBegin (GL_LINES);
            glVertex2i(circCtr.x, circCtr.y);
            glVertex2i(piePt.x, piePt.y);
        glEnd ( );

        previousSliceAngle = sliceAngle;
    }
}

void displayFcn (void)
{
    glClear (GL_COLOR_BUFFER_BIT); //  Clear display window.

    glColor3f (0.0, 0.0, 1.0); //  Set circle color to blue.

    glPushMatrix(); // Store unscaled matrix
    glScalef((GLfloat)zoomVal, (GLfloat)zoomVal, (GLfloat)zoomVal);

    switch (chartType)
    {
        case PIE_CHART:
            pieChart();
            break;
        case LINE_CHART:
            lineChart();
            break;
        case BAR_CHART:
            barChart();
            break;
    }

    glPopMatrix(); // Restore unscaled matrix

    glutSwapBuffers();
}

void winReshapeFcn (int newWidth, int newHeight)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, newWidth, newHeight);
    gluOrtho2D(0.0, GLdouble(newWidth), 0.0, GLdouble(newHeight));

    /*  Reset display-window size parameters.  */
    winWidth = newWidth;
    winHeight = newHeight;    

    glutPostRedisplay();
}

void quit(void)
{
    exit(0);
}

void zoom(bool in)
{
    if (in)
    {
        zoomVal *= 2;
    }
    else
    {
        zoomVal /= zoomVal == 1 ? 1 : 2;
        offsetX = 0;
        offsetY = 0;
    }
}

void chart_menu(int id)
{
    chartType = id;
}

void zoom_menu(int id)
{
    switch (id)
    {
        case 0:
            zoom(true);
            break;
        case 1:
            zoom(false);
            break;
        case 2:
            offsetX -= 3;
            break;
        case 3:
            offsetX += 3;
            break;
        case 4:
            offsetY -= 3;
            break;
        case 5:
            offsetY += 3;
            break;
        case 6:
            zoomVal = 1;
            offsetX = 0;
            offsetY = 0;
            break;
    }
}

void right_menu(int id)
{
    if (id == 1)
    {
        quit();
    }
}

void keyboardFcn(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 'q':
            quit();
            break;
        case 'l':
            chartType = LINE_CHART;
            break;
        case 'p':
            chartType = PIE_CHART;
            break;
        case 'b':
            chartType = BAR_CHART;
            break;
        case 'c':
            offsetX = 0;
            offsetY = 0;
            break;
        case 'z':
            zoom(true);
            break;
        case 'Z':
            zoom(false);
            break;
    }

    glutPostRedisplay();
}

void specialFcn(int key, int x, int y)
{
    switch (key)
    {
        case GLUT_KEY_RIGHT:
            offsetX -= 3;
            break;
        case GLUT_KEY_LEFT:
            offsetX += 3;
            break;
        case GLUT_KEY_UP:
            offsetY -= 3;
            break;
        case GLUT_KEY_DOWN:
            offsetY += 3;
            break;
    }

    glutPostRedisplay();
}

int main (int argc, char** argv)
{
    int c_menu, z_menu;

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(winWidth, winHeight);
    glutCreateWindow("Charts");

    c_menu = glutCreateMenu(chart_menu);
    glutAddMenuEntry("Line Chart", LINE_CHART);
    glutAddMenuEntry("Pie Chart", PIE_CHART);
    glutAddMenuEntry("Bar Chart", BAR_CHART);

    z_menu = glutCreateMenu(zoom_menu);
    glutAddMenuEntry("Zoom", 0);
    glutAddMenuEntry("Unzoom", 1);
    glutAddMenuEntry("Move Right", 2);
    glutAddMenuEntry("Move Left", 3);
    glutAddMenuEntry("Move Up", 4);
    glutAddMenuEntry("Move Down", 5);
    glutAddMenuEntry("Reset", 6);

    glutCreateMenu(right_menu);
    glutAddSubMenu("Chart Type", c_menu);
    glutAddSubMenu("Zoom", z_menu);
    glutAddMenuEntry("Quit", 1);
    glutAttachMenu(GLUT_RIGHT_BUTTON);

    init();
    glutKeyboardFunc(keyboardFcn);
    glutSpecialFunc(specialFcn);
    glutDisplayFunc(displayFcn);
    glutReshapeFunc(winReshapeFcn);

    glutMainLoop();
}
